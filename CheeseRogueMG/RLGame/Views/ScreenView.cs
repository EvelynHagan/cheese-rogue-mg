﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheeseRogueMG.RLGame.Views
{
    //View Masterclass
    static class ScreenView
    {
        static readonly int GRID_HEIGHT = 50;
        static readonly int GRID_WIDTH = 64;
        static readonly int TILE_SIZE = 16;
        static Texture2D[,] grid;
        static ScreenView()
        {
            grid = new Texture2D[GRID_WIDTH, GRID_HEIGHT];
        }
        

        public static void Clear_Grid()
        {
            grid = new Texture2D[50, 64];
        }
        public static void Draw(SpriteBatch spriteBatch)
        {
            for(int x = 0; x < GRID_WIDTH; x++)
            {
                for(int y = 0; y < GRID_HEIGHT; y++)
                {
                    if(grid[x,y] != null)
                    {
                        Rectangle rect = new Rectangle(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
                        spriteBatch.Draw(grid[x, y], rect, Color.White);
                    }
                }
            }
        }

    }
}
