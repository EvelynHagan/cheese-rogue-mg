﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CheeseRogueMG.Utilities;

namespace CheeseRogueMG.RLGame
{
    public class Actor : IActor
    {
        readonly char Representation;
        readonly string sprite_sheet;
        readonly int tile_number;
        int id;
        string name { get; set; }
        string type { get; set; }
        int x = -1;
        int y = -1;
        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set { y = value; }
        }
        public Actor(char Representation, string sprite_sheet, int tile_number, int id = -1)
        {
            this.Representation = Representation;
            this.sprite_sheet = sprite_sheet;
            this.tile_number = tile_number;
            this.id = id;
        }

        public char GetChar()
        {
            return Representation;
        }
        //public Color GetColor()
        //{
        //    return color;
        //}
        //public void WriteChar()
        //{
        //    Console.Write(Representation, color);
        //}

        public void Interact()
        {
            
        }

        public cell PositionToCell()
        {
            return new cell(X, Y);
        }

        public void UpdatePosition(cell in_cell)
        {
            X = in_cell.x;
            Y = in_cell.y;
        }

        public void UpdatePosition(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return name;
        }

        public void SetID(int id)
        {
            this.id = id;
        }

        public int GetID()
        {
            return id;
        }

        public void SetType(string type)
        {
            this.type = type;
        }

        string IActor.GetType()
        {
            return type;
        }
    }
}
