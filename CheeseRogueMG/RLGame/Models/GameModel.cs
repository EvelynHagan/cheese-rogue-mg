﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheeseRogueMG.RLGame.Actors;
using CheeseRogueMG.RLGame.World;
using CheeseRogueMG.Utilities.DungeonGeneration;

namespace CheeseRogueMG.RLGame.Models
{
    public class GameModel
    {
        public Dungeon dungeon { get; set; }
        public Character pc { get; set; }
        public WorldModel world { get; set; }
    }
}
