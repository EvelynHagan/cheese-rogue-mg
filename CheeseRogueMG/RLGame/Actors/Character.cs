﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace CheeseRogueMG.RLGame.Actors
{
    public enum CharacterClass
    {
        Warrior = 1,
        Mage = 2,
        Thief = 3
    }
    public class Character : Actor
    {
        static readonly char Representation = 'C';

        static readonly string type = "PlayerCharacter";
        static readonly string sprite_sheet = "char_sheet";
        static readonly int tile_number = 1;
        public Character(int input_id = -1) : base(Representation, sprite_sheet, tile_number, input_id) {
            SetType(type);
        }

        
        public CharacterClass characterClass { get; set; }
        public Pronouns pronouns { get; set; }


        //     Stats
        int Power { get; set; }
        int Endurance { get; set; }
        int Swiftness { get; set; }
        int Knowledge { get; set; }
        int Spirituality { get; set; }
        int Will { get; set; }

    }
}
