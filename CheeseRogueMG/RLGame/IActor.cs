﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CheeseRogueMG.Utilities;

namespace CheeseRogueMG.RLGame
{
    public interface IActor
    {
        cell PositionToCell();
        void UpdatePosition(cell in_cell);
        void UpdatePosition(int x, int y);
        char GetChar();
        void Interact();

        void SetName(string name);
        string GetName();
        void SetID(int id);
        int GetID();
        void SetType(string type);
        string GetType();
    }
}
