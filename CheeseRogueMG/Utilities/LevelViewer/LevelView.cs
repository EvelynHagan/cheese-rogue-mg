﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheeseRogueMG.Utilities.LevelGeneration;
namespace CheeseRogueMG.Utilities.LevelViewer
{
    class LevelView
    {
        public int width { get; set; }
        public int height { get; set; }

        public int upper_left_x { get; set; }
        public int upper_left_y { get; set; }

        public int x_bound { get; set; }
        public int y_bound { get; set; }

        public LevelView()
        {
            width = 75;
            height = 45;

            upper_left_x = 1;
            upper_left_y = 1;
        }

        public void MoveView(Direction direction)
        {
            cell start = new cell(upper_left_x, upper_left_y);
            cell updated = Directional.GetDirectionalCell(direction, start);
            upper_left_x = Math.Max(0, Math.Min(updated.x, x_bound - width));
            upper_left_y = Math.Max(0, Math.Min(updated.y, y_bound - height));
        }


        public void DrawLevel(Level level, bool clear = true)
        {
            throw new NotImplementedException();
        }

        public void WriteFormattedLevelString(Level level, bool clear = true)
        {
            throw new NotImplementedException();
        }

        public void CenterOnCell(cell target)
        {
            int tmp_upper_left_x = target.x - (width / 2);
            int tmp_upper_left_y = target.y - (height / 2);
            upper_left_x = Math.Max(0, Math.Min(tmp_upper_left_x, x_bound - width));
            upper_left_y = Math.Max(0, Math.Min(tmp_upper_left_y, y_bound - height));
        }
    }
}
