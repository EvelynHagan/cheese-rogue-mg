﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CheeseRogueMG.RLGame;
using CheeseRogueMG.RLGame.Actors;
using CheeseRogueMG.Utilities.FloorGeneration;
using CheeseRogueMG.Utilities.FloorViewer;
using CheeseRogueMG.Utilities.LevelGeneration;

namespace CheeseRogueMG.Utilities.LevelViewer
{
    public class LevelViewController
    {
        LevelView view;
        Level level;
        public LevelViewController()
        {
            
            FloorGenerator floor_gen = new FloorGenerator();
            FloorGeneratorConfig config = new FloorGeneratorConfig();
            config.MapSizeX = 30;
            config.MapSizeY = 30;
            config.RoomMinWidth = 3;
            config.RoomMaxWidth = 5;
            config.RoomMinHeight = config.RoomMinWidth;
            config.RoomMaxHeight = config.RoomMaxWidth;
            Floor floor = floor_gen.generate_floor(config);
            LevelGenerator level_gen = new LevelGenerator(new Random());
            level = level_gen.GenerateLevel(floor);
            Character character = new Character();
            level.SpawnPlayer(character);
            view = new LevelView();
            view.x_bound = level.floor_ref.floor_map.GetLength(0);
            view.y_bound = level.floor_ref.floor_map.GetLength(1);
        }
        public LevelViewController(Level level)
        {
            this.level = level;
            view = new LevelView();
            view.x_bound = level.floor_ref.floor_map.GetLength(0);
            view.y_bound = level.floor_ref.floor_map.GetLength(1);
        }

        public void CenterOnPlayer()
        {
            IActor pc = level.GetPlayer();
            if (pc != null)
                view.CenterOnCell(pc.PositionToCell());

        }

        public void RunDebug()
        {
            while (true)
            {
                DrawView();
                if (ReadInput())
                    return;
            }
        }
        

        bool ReadInput()
        {
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            switch (keyInfo.Key)
            {
                case ConsoleKey.UpArrow:
                case ConsoleKey.W:
                    view.MoveView(Direction.North);
                    break;
                case ConsoleKey.DownArrow:
                case ConsoleKey.S:
                    view.MoveView(Direction.South);
                    break;
                case ConsoleKey.LeftArrow:
                case ConsoleKey.A:
                    view.MoveView(Direction.West);
                    break;
                case ConsoleKey.RightArrow:
                case ConsoleKey.D:
                    view.MoveView(Direction.East);
                    break;
                case ConsoleKey.Escape:
                    return true;
            }
            return false;
        }
        public void DrawView()
        {
            view.WriteFormattedLevelString(level);
        }
    }
}
