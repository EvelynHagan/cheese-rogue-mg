﻿using System;
using System.Collections.Generic;
using System.Text;
using CheeseRogueMG.Utilities.FloorGeneration;

namespace CheeseRogueMG.Utilities
{
    public enum Direction
    {
        North,
        South,
        East,
        West
    }
    public class Directional
    {
        public static cell GetDirectionalCell(Direction dir, cell in_cell)
        {
            switch(dir)
            {
                case Direction.East:
                    return new cell(in_cell.x + 1, in_cell.y);
                case Direction.North:
                    return new cell(in_cell.x, in_cell.y - 1);
                case Direction.South:
                    return new cell(in_cell.x, in_cell.y + 1);
                case Direction.West:
                    return new cell(in_cell.x - 1, in_cell.y);
                default:
                    return in_cell;
            }
        }
    }
}
