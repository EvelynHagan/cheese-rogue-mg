﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace CheeseRogueMG.Utilities
{
    public class CharColor
    {
        public Microsoft.Xna.Framework.Color color { get; set; }
        public char representation { get; set; }
        public CharColor(Microsoft.Xna.Framework.Color color, char representation)
        {
            this.color = color;
            this.representation = representation;
        }
    }
}
