﻿using System;
using System.Collections.Generic;
using System.Text;
using CheeseRogueMG.Utilities.FloorGeneration;

namespace CheeseRogueMG.Utilities.LevelGeneration
{
    class LevelGenerator
    {
        Random random;
        public LevelGenerator(Random random)
        {
            this.random = random;
        }
        public LevelGenerator(int seed = -1)
        {
            if (seed == -1)
                seed = System.DateTime.Now.Millisecond;
            this.random = new Random(seed);
        }
        public Level GenerateLevel(Floor floor)
        {
            Level level = new Level(floor);

            PlaceSpawnPoint(floor, level);

            return level;
        }

        void PlaceSpawnPoint(Floor floor, Level level)
        {
            List<cell> options = new List<cell>();
            for(int x=1; x < floor.floor_map.GetLength(0)-1; x++ )
            {
                for(int y=1; y < floor.floor_map.GetLength(1)-1; y++)
                {
                    if (cell_valid_spawn(floor, new cell(x, y)))
                        options.Add(new cell(x,y));
                }
            }
            level.SetPlayerSpawnPoint(options[random.Next(0, options.Count)]);
        }
        bool cell_valid_spawn(Floor floor, cell in_cell)
        {
            if (in_cell.x == 0 ||
                in_cell.y == 0 ||
                in_cell.x >= floor.floor_map.GetLength(0) - 1 ||
                in_cell.y >= floor.floor_map.GetLength(1) - 1)
                return false;
            if (floor.floor_map[in_cell.x - 1, in_cell.y + 1] != LevelGenerationConstants.room_edge ||
               floor.floor_map[in_cell.x - 1, in_cell.y] != LevelGenerationConstants.room_edge ||
               floor.floor_map[in_cell.x - 1, in_cell.y - 1] != LevelGenerationConstants.room_edge ||
               floor.floor_map[in_cell.x, in_cell.y + 1] != LevelGenerationConstants.room_edge ||
               floor.floor_map[in_cell.x, in_cell.y] != LevelGenerationConstants.room_edge ||
               floor.floor_map[in_cell.x, in_cell.y - 1] != LevelGenerationConstants.room_edge ||
               floor.floor_map[in_cell.x + 1, in_cell.y + 1] != LevelGenerationConstants.room_edge ||
               floor.floor_map[in_cell.x + 1, in_cell.y] != LevelGenerationConstants.room_edge ||
               floor.floor_map[in_cell.x + 1, in_cell.y - 1] != LevelGenerationConstants.room_edge)
                return false;
            return true;

        }
    }
}
