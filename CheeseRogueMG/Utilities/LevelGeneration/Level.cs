﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CheeseRogueMG.RLGame;
using CheeseRogueMG.RLGame.Actors;
using CheeseRogueMG.Utilities.FloorGeneration;

namespace CheeseRogueMG.Utilities.LevelGeneration
{
    public class Level
    {
        public Floor floor_ref { get; set; }
        public int floor_number { get; set; }

        cell PlayerSpawnPoint;

        List<IActor> actors;

        int id_count = 0;

        Level()
        {
            actors = new List<IActor>();
        }
        public Level(Floor floor) : this()
        {
            floor_ref = floor;
        }

        public void AddActor(IActor actor)
        {
            actor.SetID(id_count);
            id_count++;
            actors.Add(actor);
        }

        public void SetPlayerSpawnPoint(cell in_cell)
        {
            PlayerSpawnPoint = in_cell;
        }

        public void SpawnPlayer(Character pc)
        {
            pc.UpdatePosition(PlayerSpawnPoint);
            AddActor(pc);
        }

        bool ActorInCell(cell in_cell)
        {
            return (actors.Find(actor => actor.PositionToCell() == in_cell)) != null;
        }

        public IActor GetActorInCell(cell in_cell)
        {
            return (actors.Find(actor => actor.PositionToCell() == in_cell));
        }

        public IActor GetPlayer()
        {
            return actors.Find(o => o.GetType() == "PlayerCharacter");
        }
        public IActor GetActorById(int id)
        {
            return actors.Find(o => o.GetID() == id);
        }

        void WriteCell(cell in_cell)
        {
            throw new NotImplementedException();
        }

        
        bool cell_empty(cell in_cell)
        {
            if (floor_ref.floor_map[in_cell.x, in_cell.y] == LevelGenerationConstants.room_edge
                && actors.Find(o => o.PositionToCell() == in_cell) == null)
                return true;
            return false;
        }
 
        public bool MoveActor(int id, Direction dir)
        {
            IActor actor = GetActorById(id);
            if(actor!=null)
            {
                cell new_cell = Directional.GetDirectionalCell(dir,actor.PositionToCell());
                if(cell_empty(new_cell))
                {
                    actor.UpdatePosition(new_cell);
                    return true;
                }
            }
            return false;
        }
    }
}
