﻿using System;
using System.Collections.Generic;
using System.Text;
using CheeseRogueMG.Utilities.FloorGeneration;
using CheeseRogueMG.Utilities.LevelGeneration;

namespace CheeseRogueMG.Utilities.DungeonGeneration
{
    public class Dungeon
    {
        public List<Level> levels { get; set; }
        public int current_level { get; set; }
        public Dungeon()
        {
            current_level = -1;
            levels = new List<Level>();
        }

        public Level GetCurrentLevel()
        {
            if (current_level < levels.Count)
                return levels[current_level];
            throw new ArgumentOutOfRangeException();
        }

    }
}
