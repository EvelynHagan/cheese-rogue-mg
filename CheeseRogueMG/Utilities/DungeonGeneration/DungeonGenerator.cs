﻿using System;
using System.Collections.Generic;
using System.Text;
using CheeseRogueMG.Utilities.FloorGeneration;
using CheeseRogueMG.Utilities.LevelGeneration;

namespace CheeseRogueMG.Utilities.DungeonGeneration
{
    class DungeonGenerator
    {
        Random random;
        public Dungeon GenerateDungeon(Dungeon dungeon, int seed = -1)
        {
            if(seed== -1)
                seed = System.DateTime.Now.Millisecond;
            random = new Random(seed);
            for (int i =0; i < 10; i++)
            {
                Floor floor = GenerateFloor();
                Level level = GenerateLevel(floor);
                dungeon.levels.Add(level);
            }
            
            return dungeon;
        }

        

        Floor GenerateFloor()
        {
            FloorGenerator generator = new FloorGenerator();
            FloorGeneratorConfig config = new FloorGeneratorConfig();

            config.deadend_passes = 100;
            config.EmptyAfterHallwayPercent = .02f;
            config.MapSizeX = random.Next(80, 150);
            config.MapSizeY = random.Next(50, 100);
            config.RoomMinHeight = random.Next(3, 7);
            config.RoomMinWidth = config.RoomMinHeight;
            int maxsize = 10;
            if (random.Next(100) > 75)
                maxsize = 20;
            config.RoomMaxHeight = random.Next(config.RoomMinHeight + 1, maxsize);
            config.RoomMaxWidth = config.RoomMaxHeight;
            config.RoomPadding = random.Next(1, 12);
            config.hallway_gen_type = Utilities.FloorGeneration.HallwayGeneration.HallwayTypes.RecursiveBacktrack;
            config.RoomPlacementAttempts = random.Next(200, 3000);
            Random floor_random = new Random(random.Next());
            config.seed = floor_random.Next(1000000000);
            config.connection_gen_types = FloorGeneration.ConnectionGeneration.ConnectionTypes.RandomConnections;
            Floor floor = generator.generate_floor(config);
            return floor;
        }
        private Level GenerateLevel(Floor floor)
        {
            LevelGenerator gen = new LevelGenerator(random.Next());
            return gen.GenerateLevel(floor);
        }

    }
}
