﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogueMG.Utilities
{
    public class cell
    {
        public int x;
        public int y;
        public cell(int ix, int iy)
        {
            x = ix;
            y = iy;
        }

        public static bool operator ==(cell a, cell b)
        {
            return a.x == b.x && a.y == b.y;
        }
        public static bool operator !=(cell a, cell b)
        {
            return a.x != b.x || a.y != b.y;
        }
    }
}
