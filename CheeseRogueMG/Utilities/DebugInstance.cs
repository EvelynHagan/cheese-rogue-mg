﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using CheeseRogueMG.Utilities.FloorGeneration;

namespace CheeseRogueMG.Utilities
{
    class DebugInstance
    {
        public void Run()
        {
            throw new NotImplementedException();
        }

        private void FloorViewerTest()
        {
            FloorViewer.FloorViewController floorViewController = new FloorViewer.FloorViewController();
            floorViewController.Run();
        }
        private void LevelViewerTest()
        {
            LevelViewer.LevelViewController levelViewController = new LevelViewer.LevelViewController();
            levelViewController.RunDebug();
        }
        private void LevelGenerationMenu()
        {
            FloorGenerator generator = new FloorGenerator();
            FloorGeneratorConfig config = new FloorGeneratorConfig();

            config.deadend_passes = 100;
            config.EmptyAfterHallwayPercent = .02f;
            Random random = new Random();
            config.seed = random.Next(1000000000);
            config.MapSizeX = random.Next(80, 150);
            config.MapSizeY = random.Next(50, 100);
            config.RoomMinHeight = random.Next(3, 7);
            config.RoomMinWidth = config.RoomMinHeight;
            int maxsize = 10;
            if (random.Next(100) > 75)
                maxsize = 20;
            config.RoomMaxHeight = random.Next(config.RoomMinHeight + 1, maxsize);
            config.RoomMaxWidth = config.RoomMaxHeight;
            config.RoomPadding = random.Next(1, 12);
            config.hallway_gen_type = FloorGeneration.HallwayGeneration.HallwayTypes.RecursiveBacktrack;
            if (random.Next() % 2 == 0)
                config.hallway_gen_type = FloorGeneration.HallwayGeneration.HallwayTypes.GrowingTree;
            config.RoomPlacementAttempts = random.Next(200, 3000);
            config.connection_gen_types = FloorGeneration.ConnectionGeneration.ConnectionTypes.RandomConnections;
            if (random.Next() % 2 == 0)
                config.connection_gen_types = FloorGeneration.ConnectionGeneration.ConnectionTypes.Standard;
            config.cleanup_types = new List<FloorGeneration.Cleanup.CleanupTypes>();
            config.cleanup_types.Add(FloorGeneration.Cleanup.CleanupTypes.ClearDeadends);
            Floor floor = generator.generate_floor(config);
            floor.DebugDraw();
            config.WriteOutConfig();
            Console.ReadKey();
        }
    }
}
