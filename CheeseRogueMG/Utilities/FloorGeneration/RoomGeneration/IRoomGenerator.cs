﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogueMG.Utilities.FloorGeneration.RoomGeneration
{
    interface IRoomGenerator
    {
        char[,] PlaceRooms(char[,] floor_map, FloorGeneratorConfig config, Random random);
    }
}
