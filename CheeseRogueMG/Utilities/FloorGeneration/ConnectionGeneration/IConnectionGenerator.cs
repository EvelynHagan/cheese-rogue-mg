﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheeseRogueMG.Utilities.FloorGeneration.ConnectionGeneration
{
    interface IConnectionGenerator
    {
        char[,] GenerateConnections(char[,] floor_map, int[,] region_map, FloorGeneratorConfig config, Random random);
    }
}
