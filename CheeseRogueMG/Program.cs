﻿using System;
using SadConsole;
using Microsoft.Xna.Framework;
using Console = SadConsole.Console;
namespace CheeseRogueMG
{
    class Program
    {
        static void Main()
        {
            // Setup the engine and create the main window.
            SadConsole.Game.Create(80, 25);

            // Hook the start event so we can add consoles to the system.
            SadConsole.Game.OnInitialize = Init;

            // Start the game.
            SadConsole.Game.Instance.Run();
            SadConsole.Game.Instance.Dispose();
        }

        static void Init()
        {
            var console = new Console(80, 25);

            // X, Y, Color
            console.SetBackground(2, 2, Color.DarkGray);

            // X, Y, Glyph index
            console.SetGlyph(2, 2, 1);

            // X, Y, Color
            console.SetForeground(2, 2, Color.DarkBlue);

            // X, Y, Mirror set
            console.SetMirror(2, 2, Microsoft.Xna.Framework.Graphics.SpriteEffects.FlipVertically);

            // Start XY, End XY, Foreground color, Background color, Glyph character
            console.DrawLine(new Point(2, 4), new Point(20, 6), Color.BlueViolet, Color.White, 4);

            console.DrawCircle(new Rectangle(10, 10, 8, 4), new Cell(Color.BlueViolet, Color.White, 4));

            console.DrawBox(new Rectangle(10, 17, 8, 4), new Cell(Color.BlueViolet, Color.White, 4));

            SadConsole.Global.CurrentScreen = console;
        }
    }
}
